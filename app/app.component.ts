import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    template: `
    <div class="root">
    <h1 id="header">Nordic Consultant</h1>
        <div id="navbar">
         <a class="navlink" href="/">
            Home
        </a>
         <a class="navlink" href="/high-score">
            High-score
        </a>
    </div>
    <div id="content">
        <router-outlet></router-outlet>
        </div>

        <div id="footer">
        Footer
    </div>
        </div>
        `
})


export class AppComponent {
}
