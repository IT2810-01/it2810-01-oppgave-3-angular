import { Injectable }    from '@angular/core';

var initial_state = [
    {
        name: "Dag Frode",
        xp: 14000,
        frontendxp: 5000,
        backendxp: 6000,
        maintanancexp: 3000,
        id: 1
    },
    {
        name: "Dag Frode2",
        xp: 21,
        frontendxp: 10,
        backendxp: 1,
        maintanancexp: 10,
        id: 2
    },
    {
        name: "Frode",
        xp: 2100,
        frontendxp: 1000,
        backendxp: 1000,
        maintanancexp: 100,
        id: 3
    },
    {
        name: "Dag Frode4",
        xp: 450,
        frontendxp: 200,
        backendxp: 150,
        maintanancexp: 100,
        id: 4
    },
    {
        name: "Dag Frode5",
        xp: 300,
        frontendxp: 100,
        backendxp: 100,
        maintanancexp: 100,
        id: 5
    },
    {
        name: "Dag Frode6",
        xp: 260,
        frontendxp: 170,
        backendxp: 30,
        maintanancexp: 60,
        id: 6
    }
];


@Injectable()
export class HighScoreService {

    getHighScores() {
        return initial_state;
    }

}