import { Component } from '@angular/core';
import { HighScoreService } from './high-score.service'

@Component({
    template: `
     <div id="highscore">
        <div class="row">
            <div class="col s12 m2">Name</div>
            <div class="col s3 m2">EXP</div>
            <div class="col s3 m2">Frontent</div>
            <div class="col s3 m2">Backend</div>
            <div class="col s3 m2">Maintanance</div>
        </div>
        <div *ngFor="let user of high_scores" class="user-container row">
            <div class="col s12 m2">{{user.name}}</div>
            <div class="col s3 m2">  {{user.xp}}</div>
            <div class="col s3 m2">  {{user.frontendxp}}</div>
            <div class="col s3 m2">  {{user.backendxp}}</div>
            <div class="col s3 m2">  {{user.maintanancexp}}</div>
        </div>
    </div>`
})

export class HighScoreListComponent {
    private high_scores:any;

    constructor(private highScoreService:HighScoreService) {
    }
}