"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var initial_state = [
    {
        name: "Dag Frode",
        xp: 14000,
        frontendxp: 5000,
        backendxp: 6000,
        maintanancexp: 3000,
        id: 1
    },
    {
        name: "Dag Frode2",
        xp: 21,
        frontendxp: 10,
        backendxp: 1,
        maintanancexp: 10,
        id: 2
    },
    {
        name: "Frode",
        xp: 2100,
        frontendxp: 1000,
        backendxp: 1000,
        maintanancexp: 100,
        id: 3
    },
    {
        name: "Dag Frode4",
        xp: 450,
        frontendxp: 200,
        backendxp: 150,
        maintanancexp: 100,
        id: 4
    },
    {
        name: "Dag Frode5",
        xp: 300,
        frontendxp: 100,
        backendxp: 100,
        maintanancexp: 100,
        id: 5
    },
    {
        name: "Dag Frode6",
        xp: 260,
        frontendxp: 170,
        backendxp: 30,
        maintanancexp: 60,
        id: 6
    }
];
var HighScoreService = (function () {
    function HighScoreService() {
    }
    HighScoreService.prototype.getHighScores = function () {
        return initial_state;
    };
    HighScoreService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], HighScoreService);
    return HighScoreService;
}());
exports.HighScoreService = HighScoreService;
//# sourceMappingURL=high-score.service.js.map