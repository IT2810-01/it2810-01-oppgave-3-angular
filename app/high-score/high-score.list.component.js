"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var high_score_service_1 = require('./high-score.service');
var HighScoreListComponent = (function () {
    function HighScoreListComponent(highScoreService) {
        this.highScoreService = highScoreService;
    }
    HighScoreListComponent = __decorate([
        core_1.Component({
            template: "\n     <div id=\"highscore\">\n        <div class=\"row\">\n            <div class=\"col s12 m2\">Name</div>\n            <div class=\"col s3 m2\">EXP</div>\n            <div class=\"col s3 m2\">Frontent</div>\n            <div class=\"col s3 m2\">Backend</div>\n            <div class=\"col s3 m2\">Maintanance</div>\n        </div>\n        <div *ngFor=\"let user of high_scores\" class=\"user-container row\">\n            <div class=\"col s12 m2\">{{user.name}}</div>\n            <div class=\"col s3 m2\">  {{user.xp}}</div>\n            <div class=\"col s3 m2\">  {{user.frontendxp}}</div>\n            <div class=\"col s3 m2\">  {{user.backendxp}}</div>\n            <div class=\"col s3 m2\">  {{user.maintanancexp}}</div>\n        </div>\n    </div>"
        }), 
        __metadata('design:paramtypes', [high_score_service_1.HighScoreService])
    ], HighScoreListComponent);
    return HighScoreListComponent;
}());
exports.HighScoreListComponent = HighScoreListComponent;
//# sourceMappingURL=high-score.list.component.js.map