import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { routing } from './app.routes';

import { AppComponent }  from './app.component';
import { HomeComponent }  from './home/home.component';
import { HighScoreService }  from './high-score/high-score.service';
import { HighScoreListComponent } from "./high-score/high-score.list.component";

@NgModule({
    imports: [
        BrowserModule,
        routing
    ]
    ,
    declarations: [
        AppComponent,
        HomeComponent,
        HighScoreListComponent
    ],
    bootstrap: [AppComponent],
    providers: [
        HighScoreService
    ]
})

export class AppModule {
}

