import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { HighScoreListComponent } from './high-score/high-score.list.component';

// Route Configuration
export const routes:Routes = [
    {path: '', component: HomeComponent},
    {path: 'high-score', component: HighScoreListComponent}
];

export const routing:ModuleWithProviders = RouterModule.forRoot(routes);